<?php

namespace app\assets\fonts;

use yii\web\AssetBundle;

/**
 * Class ProximaNovaAsset
 * @package app\assets\fonts
 */
class ProximaNovaAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/fonts/proxima';

    public $css = [];

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public function init()
    {
        $this->css = [
            'css/stylesheet.css?' . time(),
        ];
    }
}