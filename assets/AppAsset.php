<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class AppAsset
 * @package app\assets
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [];

    public $js = [
    ];
    public $depends = [
        'linex\modules\main\assets\Bootstrap4Asset',
        'linex\modules\main\assets\FontAwesomeAsset',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public function init()
    {
        $this->css = [
            'css/site.css?' . time(),
        ];
    }
}
