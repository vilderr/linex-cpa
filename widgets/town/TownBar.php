<?php

namespace app\widgets\town;

use app\widgets\town\assets\TownBarAsset;
use yii\base\Widget;

/**
 * Class TownBarWidget
 * @package app\widgets\town
 */
class TownBar extends Widget
{
    public function run()
    {
        TownBarAsset::register($this->getView());

        return $this->render('townbar');
    }
}