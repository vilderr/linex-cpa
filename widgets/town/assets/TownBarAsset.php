<?php

namespace app\widgets\town\assets;

use yii\web\AssetBundle;

/**
 * Class TownBarAsset
 * @package app\widgets\town\assets
 */
class TownBarAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/town/assets/dist';

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public function init()
    {
        $this->css = [
            'css/townbar.css?' . time(),
        ];
    }
}