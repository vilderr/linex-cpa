<?php

namespace app\widgets\social\assets;

use yii\web\AssetBundle;

/**
 * Class SocailIconsAsset
 * @package app\widgets\social\assets
 */
class SocailIconsAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/social/assets/dist';

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public function init()
    {
        $this->css = [
            'css/icons.css?' . time(),
        ];
    }
}