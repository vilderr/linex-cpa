<?php

namespace app\widgets\social;

use app\widgets\social\assets\SocailIconsAsset;
use yii\base\Widget;

/**
 * Class SocialIcons
 * @package app\widgets\social
 */
class SocialIcons extends Widget
{
    public function run()
    {
        SocailIconsAsset::register($this->getView());
        return $this->render('icons');
    }
}