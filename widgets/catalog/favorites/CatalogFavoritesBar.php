<?php

namespace app\widgets\catalog\favorites;

use app\widgets\catalog\favorites\assets\CatalogFavoritesBarAsset;
use yii\base\Widget;

/**
 * Class CatalogFavoritesBar
 * @package app\widgets\catalog\favorites
 */
class CatalogFavoritesBar extends Widget
{
    public function run()
    {
        CatalogFavoritesBarAsset::register($this->getView());

        return $this->render('bar');
    }
}