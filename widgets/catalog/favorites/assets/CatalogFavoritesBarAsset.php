<?php

namespace app\widgets\catalog\favorites\assets;

use yii\web\AssetBundle;

/**
 * Class CatalogFavoritesAsset
 * @package app\widgets\catalog\favorites\assets
 */
class CatalogFavoritesBarAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/catalog/favorites/assets/dist';

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public function init()
    {
        $this->css = [
            'css/favoritesbar.css?' . time(),
        ];
    }
}