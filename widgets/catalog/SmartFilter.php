<?php

namespace app\widgets\catalog;

use app\widgets\catalog\assets\SmartFilterAsset;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class SmartFilter
 * @package app\widgets\catalog
 */
class SmartFilter extends Widget
{
    public $model;

    public function run()
    {
        SmartFilterAsset::register($this->view);

        echo $this->renderItems();
    }

    private function renderItems()
    {
        $items = [];

        foreach ($this->model->items as $item)
        {
            if ($result = $this->renderItem($item))
            {
                $items[] = $result;
            }
        }

        return Html::tag('div', implode("\n", $items), ['class' => 'widget smartfilter']);
    }

    private function renderItem(array $item)
    {
        if (!is_array($item['variants']) || empty($item['variants']))
            return null;

        $ar = [];
        $ar[] = Html::tag('div', $item['name'], ['class' => 'h6']);

        $values = [];


        foreach ($item['variants'] as $value)
        {
            $content = '';
            $content .= Html::tag('a', $value['name'], ['href' => $value['link'], 'class' => $value['checked'] ? 'text-primary active' : 'text-dark'])."\n";
            //$content .= Html::tag('small', $value['count']);
            $values[] = Html::tag('li', $content);
        }

        $ar[] = Html::tag('ul', implode("\n", $values), ['class' => 'list-unstyled list', 'data-skimpy' => $item['skimpy'] ? 'true' : 'false']);

        return Html::tag('div', implode("\n", $ar), ['class' => 'section']);
    }
}