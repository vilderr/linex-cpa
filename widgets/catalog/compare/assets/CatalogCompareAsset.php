<?php

namespace app\widgets\catalog\compare\assets;

use yii\web\AssetBundle;

/**
 * Class CatalogCompareAsset
 * @package app\widgets\catalog\compare\assets
 */
class CatalogCompareAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/catalog/compare/assets/dist';

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public function init()
    {
        $this->css = [
            'css/comparebar.css?' . time(),
        ];
    }
}