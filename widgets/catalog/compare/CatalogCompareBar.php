<?php

namespace app\widgets\catalog\compare;

use app\widgets\catalog\compare\assets\CatalogCompareAsset;
use yii\base\Widget;

/**
 * Class CatalogCompareBar
 * @package app\widgets\catalog\compare
 */
class CatalogCompareBar extends Widget
{
    public function run()
    {
        CatalogCompareAsset::register($this->getView());

        return $this->render('bar');
    }
}