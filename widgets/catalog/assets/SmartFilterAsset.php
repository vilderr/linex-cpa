<?php

namespace app\widgets\catalog\assets;

use yii\web\AssetBundle;

/**
 * Class SmartFilterAsset
 * @package app\widgets\catalog\assets
 */
class SmartFilterAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/catalog/assets/dist';

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public function init()
    {
        $this->css = [
            'css/smartfilter.css?' . time(),
        ];
    }
}