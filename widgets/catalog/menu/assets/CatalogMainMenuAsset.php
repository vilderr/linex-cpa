<?php

namespace app\widgets\catalog\menu\assets;

use yii\web\AssetBundle;

/**
 * Class CatalogMainMenuAsset
 * @package app\widgets\catalog\menu\assets
 */
class CatalogMainMenuAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/catalog/menu/assets/dist';

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public function init()
    {
        $this->css = [
            'css/mainmenu.css?' . time(),
        ];

        $this->js = [
            'js/mainmenu.js?' . time(),
        ];
    }
}