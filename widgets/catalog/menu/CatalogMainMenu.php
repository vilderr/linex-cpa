<?php

namespace app\widgets\catalog\menu;

use app\widgets\catalog\menu\assets\CatalogMainMenuAsset;
use yii\base\Widget;

/**
 * Class CatalogMainMenu
 * @package app\widgets\catalog\menu
 */
class CatalogMainMenu extends Widget
{
    public function run()
    {
        CatalogMainMenuAsset::register($this->getView());
        return $this->render('mainmenu');
    }
}