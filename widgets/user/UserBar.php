<?php

namespace app\widgets\user;

use app\widgets\user\assets\UserBarAsset;
use yii\base\Widget;

/**
 * Class UserBar
 * @package app\widgets\user
 */
class UserBar extends Widget
{
    public function run()
    {
        UserBarAsset::register($this->getView());

        return $this->render('userbar');
    }
}