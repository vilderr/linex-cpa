<?php

namespace app\widgets\user\assets;

use yii\web\AssetBundle;

/**
 * Class UserBarAsset
 * @package app\widgets\user\assets
 */
class UserBarAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/user/assets/dist';

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public function init()
    {
        $this->css = [
            'css/userbar.css?' . time(),
        ];
    }
}