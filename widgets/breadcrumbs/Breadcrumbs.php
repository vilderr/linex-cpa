<?php

namespace app\widgets\breadcrumbs;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\base\InvalidConfigException;

/**
 * Class Breadcrumb
 * @package app\widgets\breadcrumbs
 */
class Breadcrumbs extends \yii\widgets\Breadcrumbs
{
    /**
     * Renders a single breadcrumb item.
     *
     * @param array  $link     the link to be rendered. It must contain the "label" element. The "url" element is
     *                         optional.
     * @param string $template the template to be used to rendered the link. The token "{link}" will be replaced by the
     *                         link.
     *
     * @return string the rendering result
     * @throws InvalidConfigException if `$link` does not have "label" element.
     */
    protected function renderItem($link, $template)
    {
        $encodeLabel = ArrayHelper::remove($link, 'encode', $this->encodeLabels);
        if (array_key_exists('label', $link)) {
            $label = $encodeLabel ? Html::encode($link['label']) : $link['label'];
        } else {
            throw new InvalidConfigException('The "label" element is required for each link.');
        }
        if (isset($link['template'])) {
            $template = $link['template'];
        }

        if (isset($link['url'])) {
            $options = $link;
            unset($options['template'], $options['label'], $options['url']);
            $link = Html::a($label, $link['url'], $options). '<i class="divider fa fa-angle-right" aria-hidden="true"></i>';
        } else {
            $link = $label;
        }

        return strtr($template, ['{link}' => $link]);
    }
}