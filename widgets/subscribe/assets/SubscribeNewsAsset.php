<?php

namespace app\widgets\subscribe\assets;

use yii\web\AssetBundle;

/**
 * Class SubscribeNewsAsset
 * @package app\widgets\subscribe\assets
 */
class SubscribeNewsAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/subscribe/assets/dist';

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public function init()
    {
        $this->css = [
            'css/news.css?' . time(),
        ];
    }
}