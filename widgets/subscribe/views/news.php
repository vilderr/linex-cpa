<?php
?>
<div class="input-group input-group-lg" id="news_subscribe">
    <input class="form-control form-control-lg" placeholder="<?= Yii::t('app', 'Your email'); ?>"/>
    <span class="input-group-btn">
        <button class="btn btn-white text-secondary" type="button"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
    </span>
</div>
