<?php

namespace app\widgets\subscribe;

use app\widgets\subscribe\assets\SubscribeNewsAsset;
use yii\base\Widget;

/**
 * Class SubscribeNews
 * @package app\widgets\subscribe
 */
class SubscribeNews extends Widget
{
    public function run()
    {
        SubscribeNewsAsset::register($this->getView());

        return $this->render('news');
    }
}