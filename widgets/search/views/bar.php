<?php
?>
<div id="search_bar">
    <div class="input-group input-group-lg">
        <input type="text" class="form-control search__input" placeholder="<?= Yii::t('app', 'Search products'); ?>" aria-label="<?= Yii::t('app', 'Search products'); ?>">
        <div class="input-group-btn">
            <button type="button" class="btn btn-white dropdown-toggle search__btn" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                <span>Категория товаров</span> <i class="fa fa-angle-down caret" aria-hidden="true"></i>
            </button>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <a class="dropdown-item" href="#">Something</a>
            </div>
        </div>
        <span class="input-group-btn">
            <button class="btn btn-light search__btn" type="button"><?= Yii::t('app', 'Search')?></button>
        </span>
    </div>
</div>
