<?php

namespace app\widgets\search\assets;

use yii\web\AssetBundle;

/**
 * Class SearchBarAsset
 * @package app\widgets\search\assets
 */
class SearchBarAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/search/assets/dist';

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public function init()
    {
        $this->css = [
            'css/searchbar.css?' . time(),
        ];
    }
}