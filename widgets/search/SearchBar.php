<?php

namespace app\widgets\search;

use app\widgets\search\assets\SearchBarAsset;
use yii\base\Widget;

/**
 * Class SearchBar
 * @package app\widgets\search
 */
class SearchBar extends Widget
{
    public function run()
    {
        SearchBarAsset::register($this->getView());

        return $this->render('bar');
    }
}