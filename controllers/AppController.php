<?php
namespace app\controllers;

use linex\modules\dashboard\components\DashboardController;
use linex\modules\dashboard\Module as DashboardModule;
use Yii;
use yii\web\Controller;

/**
 * Class AppController
 * @package app\controllers
 */
class AppController extends Controller
{
    public function init()
    {
        $length = strlen('dashboard');
        if (substr(\Yii::$app->request->pathInfo, 0, $length) === 'dashboard')
        {
            Yii::$app->layout = 'main';
            Yii::$app->layoutPath = '@vendor/linex/modules/dashboard/views/layouts';
        }
    }

    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            return $this->render('error', ['exception' => $exception]);
        }
    }
}