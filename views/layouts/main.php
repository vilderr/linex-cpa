<?php
/**
 * @var \yii\web\View $this
 * @var string        $content
 */

use yii\helpers\ArrayHelper;
use app\widgets\breadcrumbs\Breadcrumbs;

?>
<?php $this->beginContent('@app/views/layouts/layout.php'); ?>
<div class="container">
    <?= Breadcrumbs::widget([
        'tag'                => 'ol',
        'homeLink'           => [
            'label' => 'Главная',
            'url'   => '/',
        ],
        'links'              => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'itemTemplate'       => "<li class=\"breadcrumb-item\">{link}</li>\n",
        'activeItemTemplate' => "<li class=\"breadcrumb-item active\">{link}</li>\n",
    ]); ?>
    <div class="page__header">
        <h1><?= $this->title; ?> <small><?//= $this->params['category']['product_count']?></small></h1>
    </div>
    <?= $content; ?>
</div>
<?php $this->endContent(); ?>
