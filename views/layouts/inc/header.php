<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;
use app\widgets;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div id="wrapper">
    <div id="content-wrapper">
        <header class="container">
            <div class="wrapper">
                <div class="row top__header align-items-end">
                    <div class="col-md-5">
                        <div class="logo">
                            <a href="/"><img class="img-fluid" src="/images/logo.png"/></a>
                        </div>
                    </div>
                    <div class="col-md-9 ml-auto">
                        <?= widgets\town\TownBar::widget(); ?>
                    </div>
                    <div class="col-md-9">
                        <div class="d-flex">
                            <div class="ml-auto mr-4">
                                <?= widgets\catalog\favorites\CatalogFavoritesBar::widget(); ?>
                            </div>
                            <div class="mr-4">
                                <?= widgets\catalog\compare\CatalogCompareBar::widget(); ?>
                            </div>
                            <div>
                                <?= widgets\user\UserBar::widget(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row bottom__header mb-3">
                    <div class="col-md-5 menu__box">
                        <?= widgets\catalog\menu\CatalogMainMenu::widget(); ?>
                    </div>
                    <div class="col-md-18 ml-auto">
                        <?= widgets\search\SearchBar::widget(); ?>
                    </div>
                </div>
            </div>
        </header>