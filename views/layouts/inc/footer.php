<?php
/**
 * @var \yii\web\View $this
 * @var string        $content
 */

use app\widgets;
use app\assets\AppAsset;

AppAsset::register($this);
?>
</div>
<div id="footer-wrapper">
    <section id="about_project" class="pt-4 pb-4">
        <div class="container">
            <div class="h2 text-uppercase"><?= Yii::t('app', 'About Project'); ?></div>
            <div class="row justify-content-between">
                <div class="col-md-15">
                    <p>Выбрав товары на «Мегамол», вы можете сразу оформить на них заказ. Вам не потребуется переходить
                        на сайты магазинов и регистрироваться на каждом из них. Добавляйте товары из разных магазинов в
                        единую корзину и оформляйте заказы в один прием.</p>
                    <p>Для тех товаров, которые можно заказать прямо на «Мегамол», отображается кнопка В корзину. Вы
                        можете отобрать такие товары, воспользовавшись фильтром Заказать на Маркете на странице Цены
                        карточки модели или в результатах поиска.</p>
                    <p class="mb-0">
                        <a href="#" class="btn btn-lg btn-white mr-3"><?= Yii::t('app', 'About Project') ?></a>
                        <a href="#" class="btn btn-lg btn-white"><?= Yii::t('app', 'Cooperation') ?></a>
                    </p>
                </div>
                <div class="col-md-8">
                    <p class="lead">Для каждого вида доставки указана предварительная стоимость. Она может измениться
                        после того, как вы укажете адрес доставки.</p>
                </div>
            </div>
        </div>
    </section>
    <div class="footer container">
        <div class="row justify-content-between">
            <div class="col-md-16">
                <div class="title"><?= Yii::t('app', 'Categories'); ?></div>
            </div>
            <div class="col-md-6">
                <div class="social__box mb-5">
                    <div class="title"><?= Yii::t('app', 'Social links') ?></div>
                    <?= widgets\social\SocialIcons::widget(); ?>
                </div>
                <div class="subscribe__box">
                    <div class="title"><?= Yii::t('app', 'Subscribe to news') ?></div>
                    <?= widgets\subscribe\SubscribeNews::widget(); ?>
                </div>
            </div>
        </div>
        <div class="text-center pt-5">
            <p class="mb-4"><img src="/images/foot__logo.png" class="img-fluid"/></p>
            <p class="text-dark">Copyright © 1995-2017 «Bigmall». Все права защищены.</p>
            <div class="d-flex justify-content-center">
                <div class="pr-3">
                    <a href="#" class="text-dark">Пользовательское соглашение</a>
                </div>
                <div class="pr-3">
                    <a href="#" class="text-dark">Защита конфиденциальности</a>
                </div>
                <div>
                    <a href="#" class="text-dark">Файлы cookie и AdChoice</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
