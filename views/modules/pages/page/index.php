<?php
/**
 * @var $model \linex\modules\pages\models\Page
 */
?>
<div class="page-content">
    <?= $model->content; ?>
</div>
