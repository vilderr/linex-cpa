<?php
use yii\widgets\LinkPager;
use linex\modules\catalog\models\Category;
use linex\modules\catalog\models\Product;
/**
 * @var $this         \yii\web\View
 * @var $model        \linex\modules\catalog\models\Category
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
?>
<?= $model->name; ?>
<div class="row">
<? foreach ($dataProvider->models as $product): ?>
    <div class="col-md-3">
        <div class="title"><?= $product->name; ?></div>
    </div>
<? endforeach; ?>
</div>
<div class="row">
    <div class="col-sm-6 text-left">
        <?= LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
            'maxButtonCount' => 7,
        ]) ?>
    </div>
    <div class="col-sm-6 text-right">Showing <?= $dataProvider->getCount() ?> of <?= $dataProvider->getTotalCount() ?></div>
</div>
