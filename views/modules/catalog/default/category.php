<?php
use yii\widgets\LinkPager;
use app\widgets\catalog\SmartFilter;

/**
 * @var $this         \yii\web\View
 * @var $model        \linex\modules\catalog\models\Category
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $smartFilter  \linex\modules\catalog\models\SmartFilter
 */
?>
<div class="row">
    <div class="col-md-20 order-2">
        <div class="row">
            <? foreach ($dataProvider->models as $product): ?>
                <div class="col-md-6">
                    <div class="title"><?= $product->name; ?></div>
                </div>
            <? endforeach; ?>
        </div>
        <?
        //echo '<pre>'; print_r($dataProvider->getPagination()); echo '</pre>';
        ?>
        <?= LinkPager::widget([
            'pagination'     => $dataProvider->getPagination(),
            'maxButtonCount' => 7,
        ]) ?>
    </div>
    <div class="col-md-4 order-1">
        <?= SmartFilter::widget([
            'model' => $smartFilter,
        ]); ?>
    </div>
</div>
